const path = require("path");
const HDWalletProvider = require('truffle-hdwallet-provider');
var privateKeys = [
  "1130ee5189f5626d11541266600bfe33473a4ba5d467778872a17fa76bcc0526",
  "37e59dbc50c67e73c4ce3741b63200db3f41dac9dd17fb99ac2fce65724169ee",
  "3b07903a721d33a73fa68be1ff534fd7cda8987331787a6df116d8657f215231",
];

//var provider = new HDWalletProvider(mnemonic, "http://173.193.92.171:30040/", 0, 5);
module.exports = {
  // See <http://truffleframework.com/docs/advanced/configuration>
  // to customize your Truffle configuration!
  contracts_build_directory: path.join(__dirname, "client/src/contracts"),
  networks: {

    private: {
      provider: function () {
        return new HDWalletProvider(privateKeys, `http://173.193.92.171:30040`, 0, 3);
      },
      network_id: "*",  // This network is yours, in the cloud.
      production: false,
    }
  }


};
